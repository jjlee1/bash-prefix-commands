#!/bin/bash

set -e

read -d '' usage <<"EOF" || true
Usage: inxephyr [OPTIONS] [--xephyr ARGS... --] DISPLAY PROGRAM [ARGS...]

Run a program in Xephyr.

Example:
inxephyr :3 xeyes

If another X server is not attached to that display, Xephyr will be started.

If -s or --start-session is passed, env var START_SESSION_COMMAND will be taken
as a command to run if Xephyr needs to be started.  Normally this would be a
command to start a window manager like startlxde, which is useful if you want
to deal with more than one window.

Xephyr will exit when the last program running on its display exits.

If the defaults are not right for you, use --xephyr, which replaces the default
arguments passed to Xephyr with those between `--xephyr` and the terminating
`--` sentinel.  Here's how to duplicate the default behaviour of inxephyr using
this feature:

inxephyr --xephyr -ac -screen 1280x1024 -br -reset -terminate -- :3 xeyes
EOF

# This apostrophe is here to make gitlab syntax-colour this script correctly: '

echo_to_err() {
    echo "$@" 1>&2;
}

if [[ $# == 0 ]]; then
    echo_to_err "$usage"
    exit
fi

START_SESSION_COMMAND="${START_SESSION_COMMAND:-startlxde}"

START_SESSION=0
FORWARD_XEPHYR_ARGUMENTS=0

# option arguments to forward to Xephyr
FORWARDED_ARGUMENTS=()

while [[ $# > 0 ]]
do
key="$1"
case $key in
    --help)
        echo "$usage"
        exit
        ;;
    -s|--start-session)
        START_SESSION=1
        ;;
    --xephyr)
        FORWARD_XEPHYR_ARGUMENTS=1
        ;;
    --)
        shift
        break  # end of forwarded Xephyr options
        ;;
    *)
        if [ $FORWARD_XEPHYR_ARGUMENTS -eq 1 ]; then
            FORWARDED_ARGUMENTS=("${FORWARDED_ARGUMENTS[@]}" "$key" )
        else
            break
        fi
        ;;
esac
shift
done

display="$1"
shift

if ! DISPLAY="$display" xset q >/dev/null 2>&1; then
    # nothing already running on $display
    if [ $FORWARD_XEPHYR_ARGUMENTS -eq 1 ]; then
        setsid Xephyr "${FORWARDED_ARGUMENTS[@]}" 2> /dev/null "$display" &
    else
        setsid Xephyr -ac -screen 1280x1024 -br -reset -terminate 2> /dev/null "$display" &
    fi
    # https://unix.stackexchange.com/questions/313234/how-to-launch-xephyr-without-sleep-ing
    inotifywait --timeout 10 /tmp/.X11-unix/
    if [ $START_SESSION -eq 1 ]; then
        echo_to_err Starting session: $START_SESSION_COMMAND
        DISPLAY="$display" $START_SESSION_COMMAND &
    fi
fi
exec env DISPLAY="$display" "$@"
