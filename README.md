A small collection of prefix commands implemented in the bash shell

These are simple commands that I find it's annoying to re-invent everywhere I
go, so they're here in this repo.

Some of these are useful as-is (e.g. inv, indir, inxephyr).  Others are a bit
of a hack (incolor) or lack features / may be fragile (inchroot) so you may
find you want to copy and tweak them to suit your own tasks.


## Prefix commands

A prefix command is one, like 'sudo ls', that consists of a prefix followed
some other arbitrary command.  Here are some prefixes (which I'll loosely refer
to also as 'prefix commands' -- though they will error if run because I've not
added any command like `ls` following each prefix):

```
sudo
env
chroot
echo sh -c 'cd "$1" && shift && exec "$@"' indir
```

Some reasons prefix commands are useful:

* They are easily composable:

    sudo -u bob indir /tmp inv ~/env/3/bob env SPAM=eggs python3 -c 'print(hello)'

* They make it easy to write shell commands that don't depend very much on
global bash state like current working directory, env vars, etc.

I use prefix commands at the shell prompt because it lets me run commands from
history without having to do things like cd or set env vars first.


## The commands in this project

### inchroot

Run a command in a chroot, with /proc, /sys, /dev/pts mounted, and /dev
bind-mounted to the host /dev.

Note I haven't given much attention to error cases: what happens if umount
fails, etc.  Always be careful with chroots that you don't rm -rf a chroot that
still has bind mounts (or you'll rm your bind-mounted directory along with
it!): check the output of the mount command or /proc/mounts if unsure.

TODO: Provide a way to mount other directories.


### indir

Run a command in a directory

Example:

```
indir /tmp ls
```

### inv

Run a command in a python virtual env

Example:

```
inv ~/my_env pip install nose
```

The virtual env must already exist (use the `virtualenv` command to make a
Python 2 virtual env, or `pyvenv` to make a Python 3 virtual env).

This command also sets up something like a ruby virtual env.  This is
experimental.  This is supposed to work:

```
inv ~/my_env gem install <something>
```

### inve

A convenience.  Just the same as:

```
inv ~/env/2/inve
```

Why that path?  Just my convention: my python virtual env directories have
paths like `~/env/<python major version>/<env name>`, and I use the env name
`inve` as a convenience for trying out random PyPI packages and running little
ad hoc Python scripts &c.

Example:

```
inve pip install nose
```

### inve3

Like inve but provides a separate place for Python 3 code if you want to keep
that separate from Python 2 code.  Just the same as:

```
inv ~/env/3/inve
```

Example:

```
inve3 pip3 install nose
```


### inxephyr

Usage: `inxephyr [OPTIONS] DISPLAY PROGRAM [ARGS...]`

Run a program in Xephyr.

If another X server is not attached to that display, Xephyr will be started.

Example:

```
inxephyr :3 xeyes
```

If you run that twice, you'll get two xeyes instances in the one Xephyr.

If `-s` or `--start-session` is passed, env var `START_SESSION_COMMAND` will be
taken as a command to run if Xephyr needs to be started.  Normally this would
be a command to start a window manager like startlxde, which is useful if you
want to deal with more than one window.

Xephyr will exit when the last program running on its display exits.

TODO: add an option to automatically allocate a display.  This should use
Xephyr's `-displayfd` argument, and then maybe export its *own* `--displayfd`
argument:

http://stackoverflow.com/questions/2520704/find-a-free-x11-display-number/18166600#18166600


### nocolor

Run a command removing colour output.  A regexp hack, may not be reliable!

Consider some likely better alternatives to using this:

* Pipe commands to less or to a file: often commands will disable colour output
  when their stdout is not a tty
* Use `less -R` to see colour output in less


## Some notes about famous prefix commands

`env` has `-i` (unset all) and `-u` (unset) options.  `-i` in particular can be
useful to isolate commands from your messy shell environment for
reproducibility.  Here's a way to simulate cron:

```
env -i LANGUAGE=en_GB:en HOME=/home/me LOGNAME=me PATH=/usr/bin:/bin LANG=en_GB.UTF-8 SHELL=/bin/sh PWD=/home/me your_program_here
```

Note the env vars that are set for you will depend on details of your cron
setup.  To find out what to use, add this crontab, wait one minute, then `cat
/tmp/env`:

```
* * * * * env > /tmp/env
```


## Note about commands and shell commands

To avoid confusion using prefix commands, it's useful to distinguish plain old
commands (things you can pass to `exec(3)`) from shell commands (things you can
pass to `bash -c` or type at the bash prompt).  Often we use 'command' loosely
to mean either an exec(3)-able command or a shell command -- that's fine, but
it's good to have the distinction in mind in this context.

An example of a shell command: `ls /tmp | less`.  Shell commands are strings.

An example of an exec(3)-able command (written as a python to show that it is
fundamentally an array, not a string): `['ls', '/tmp']`

Often commands are written using shell command syntax: `ls /tmp`

Of course, shell commands always contain commands (both `sudo ls` and `less`
are commands):

```
sudo ls | less
```

It is also common to use shell commands as part of a command:

```
sudo sh -c 'ls / | less'
```

Prefix commands are 'plain old' commands, not shell commands.  So when you see this:

```
sudo ls | less
sudo ls && ls /
```

The prefix command is `sudo ls`, and the `| less` or `&& ls /` are other syntax
that go to make up a bigger shell command.


## Prefix command hazards

Think about what these really mean:

```
$ indir /tmp && rm -f spam/*
$ sudo -u alice rm $HOME/spam
```

(remember the distinction between commands and shell commands explained above)

Here are some commands that might achieve what you actually meant:

```
$ indir /tmp rm -rf spam
$ indir /tmp sh -c 'rm -f spam/*'
$ sudo -u alice rm ~alice/spam
$ sudo -u alice sh -c 'rm ~$HOME/spam'
```

## How to quote strings in bash

When using `bash -c` or `sh -c` when using existing prefix commands or writing
new ones, you need to know how to quote bash commands (which are strings).

One recipe to quote a string:

0. Don't use single quotes ever
1. Replace all `\` with `\\`
2. Replace all `$` with `\$`
3. Replace all `"` with `\"`
4. Surround your new string with double quotes

The python function `pipes.quote` will do quoting for you, albeit using a
different quoting algorithm (moved to `shlex.quote` in Python 3).  If you
really want to quote a string that contains a single quote, that function will
show you how.


## How to turn a command into a shell command

1. Quote each component of the command for bash (see above)
2. Join the quoted components with the space character

As a Python function:

```
def shell_escape(args):
    return " ".join(pipes.quote(arg) for arg in args)
```

This is sometimes useful in constructing prefix commands programatically.
